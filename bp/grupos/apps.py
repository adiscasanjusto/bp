
from django.apps import AppConfig


class GruposConfig(AppConfig):
    name = "bp.grupos"
    verbose_name = "Grupos"

    def ready(self):
        """Override this to put in:
            Grupos system checks
            Grupos signal registration
        """
        try:
            import grupos.signals  # noqa F401
        except ImportError:
            pass
