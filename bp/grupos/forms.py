from django import forms

from . import models


class SeccionForm(forms.ModelForm):
    class Meta:
        model = models.Seccion
        fields = ('rama', 'unidad', 'nombre', 'lema')

    def __init__(self, grupo, *args, **kwargs):
        self.grupo = grupo
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        """Asocia la seccion con el grupo pasado por parametro."""
        seccion = super().save(commit=False)
        seccion.grupo = self.grupo
        if commit:
            seccion.save()
        return seccion
