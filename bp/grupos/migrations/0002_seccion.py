# Generated by Django 2.0.4 on 2018-04-25 21:38

import autoslug.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('grupos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Seccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rama', models.PositiveIntegerField(choices=[(1, 'Castor'), (2, 'Arcoiris'), (3, 'Lobato'), (4, 'Scout'), (5, 'Raider'), (6, 'Rover Scout')])),
                ('unidad', models.PositiveIntegerField(choices=[(0, 'Mixta'), (1, 'Femenina'), (2, 'Masculina')])),
                ('nombre', models.CharField(blank=True, max_length=255)),
                ('lema', models.CharField(blank=True, max_length=255)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='nombre')),
                ('grupo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='secciones', to='grupos.Grupo')),
            ],
        ),
    ]
