from django.db import models
from django import urls

import autoslug

from . import constants


class Grupo(models.Model):
    """Representa un grupo scout."""
    nombre = models.CharField(max_length=255)
    numero = models.IntegerField(blank=True, null=True)
    direccion = models.CharField(max_length=255, blank=True)
    email = models.EmailField(blank=True)
    telefono = models.CharField(max_length=32, blank=True)
    slug = autoslug.AutoSlugField(populate_from='nombre')
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        if not self.numero:
            return f'Grupo Scout S/N - {self.nombre}'
        return f'Grupo Scout Nº{self.numero} - {self.nombre}'

    def get_absolute_url(self):
        return urls.reverse('grupos:detail', kwargs={'slug': self.slug})


class Seccion(models.Model):
    """Representa una sección del grupo scout.

    Los integrantes del grupo scout pertenecen a una sección. Esta sección
    pertenece a una rama y puede ser unidad mixta, femenina o masculina.

    Algunos integrantes pueden pertenecer a mas de una sección:
      * Dirigentes de más una sección
      * Rovers en servicio
    """
    grupo = models.ForeignKey(Grupo, related_name='secciones',
                              on_delete=models.CASCADE)
    rama = models.PositiveIntegerField(choices=constants.RAMAS)
    unidad = models.PositiveIntegerField(choices=constants.UNIDADES)
    nombre = models.CharField(max_length=255, blank=True)
    lema = models.CharField(max_length=255, blank=True)
    slug = autoslug.AutoSlugField(populate_from='populate_slug')

    def __str__(self):
        return (f'{self.nombre} - {self.get_rama_display()} Unidad '
                f'{self.get_unidad_display()}')

    def populate_slug(self):
        """Funcion para completar el slug utilizado en la url amigable."""
        return f'{self.get_rama_display()}-{self.nombre}'

    def get_absolute_url(self):
        return urls.reverse(
            'grupos:secciones:detail',
            kwargs={'grupo_slug': self.grupo.slug, 'slug': self.slug}
        )

    def get_update_url(self):
        return urls.reverse(
            'grupos:secciones:update',
            kwargs={'grupo_slug': self.grupo.slug, 'slug': self.slug}
        )
