import django_tables2 as tables

from . import models


class GrupoTable(tables.Table):
    nombre = tables.LinkColumn()

    class Meta:
        model = models.Grupo
        fields = ('nombre', 'numero', 'direccion')


class SeccionTable(tables.Table):
    nombre = tables.LinkColumn()
    get_rama_display = tables.Column(verbose_name='Rama')
    get_unidad_display = tables.Column(verbose_name='Unidad')

    class Meta:
        model = models.Grupo
        fields = ('get_rama_display', 'get_unidad_display', 'nombre', 'lema')
