from django import shortcuts
from django.views import generic

import django_tables2

from . import (
    forms,
    models,
    tables,
)


class GrupoMixin:
    """Agrega el grupo al contexto.

    Obtiene el grupo desde el parametro ``grupo_slug` en la URL.
    """
    def get_grupo(self):
        """Obtiene el grupo desde el parametro ``grupo_slug` en la URL."""
        return shortcuts.get_object_or_404(models.Grupo,
                                           slug=self.kwargs['grupo_slug'])

    def get_context_data(self, **kwargs):
        """Agrega grupo al contexto."""
        return super().get_context_data(grupo=self.get_grupo(), **kwargs)


class GrupoListView(django_tables2.SingleTableView):
    """Muestra lista de grupos."""
    model = models.Grupo
    table_class = tables.GrupoTable
    paginate_by = 20
    ordering = ('nombre',)


class GrupoDetailView(django_tables2.SingleTableMixin, generic.DetailView):
    """Muestra detalles de un grupo. Tambien muestra las secciones"""
    model = models.Grupo
    table_class = tables.SeccionTable

    def get_table_data(self):
        """Obtiene las secciones a mostrar."""
        return self.get_object().secciones.all()



class GrupoUpdateView(generic.UpdateView):
    """Edita datos de un grupo."""
    model = models.Grupo
    fields = ('nombre', 'numero', 'direccion', 'email', 'telefono')


class GrupoCreateView(generic.CreateView):
    """Crea un grupo nuevo."""
    model = models.Grupo
    fields = ('nombre', 'numero', 'direccion', 'email', 'telefono')


class SeccionDetailView(GrupoMixin, generic.DetailView):
    """Muestra detalles de una seccion"""
    model = models.Seccion


class SeccionCreateView(GrupoMixin, generic.CreateView):
    """Crea una seccion nueva."""
    model = models.Seccion
    form_class = forms.SeccionForm

    def get_form_kwargs(self):
        data = super().get_form_kwargs()
        data['grupo'] = self.get_grupo()
        return data


class SeccionUpdateView(GrupoMixin, generic.UpdateView):
    """Edita una seccion."""
    model = models.Seccion
    fields = ('rama', 'unidad', 'nombre', 'lema')
