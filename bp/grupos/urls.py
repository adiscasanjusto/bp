from django.urls import path, include

from . import views

app_name = "grupos"

seccion_urls = ([
    path('~nuevo/', views.SeccionCreateView.as_view(), name='create'),
    path('<slug:slug>/', views.SeccionDetailView.as_view(), name='detail'),
    path('<slug:slug>/~editar/', views.SeccionUpdateView.as_view(),
         name='update'),
], 'secciones')

urlpatterns = [
    path('', views.GrupoListView.as_view(), name='list'),
    path('~nuevo/', views.GrupoCreateView.as_view(), name='create'),
    path('<slug:slug>/', views.GrupoDetailView.as_view(), name='detail'),
    path('<slug:slug>/~editar/', views.GrupoUpdateView.as_view(),
         name='update'),
    path('<slug:grupo_slug>/secciones/', include(seccion_urls))
]
