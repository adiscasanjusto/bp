import factory

from .. import constants


class GrupoFactory(factory.django.DjangoModelFactory):
    """ Crea instancias de grupos."""
    nombre = factory.Sequence(lambda n: f'Grupo Nº {n}')
    numero = factory.Faker('pyint')
    direccion = factory.Faker('address')
    email = factory.Faker('email')
    telefono = factory.Faker('phone_number')

    class Meta:
        model = 'grupos.Grupo'


class SeccionFactory(factory.django.DjangoModelFactory):
    """ Crea instancias de grupos."""
    grupo = factory.SubFactory(GrupoFactory)
    rama = factory.Iterator([x[0] for x in constants.RAMAS])
    unidad = factory.Iterator([x[0] for x in constants.UNIDADES])
    nombre = factory.Faker('company')
    lema = factory.Faker('catch_phrase')

    class Meta:
        model = 'grupos.Seccion'
