from test_plus.test import TestCase

from .. import (
    constants,
    models,
)
from . import factories


class GrupoListViewTests(TestCase):
    def test_lista_vacia(self):
        self.get('grupos:list')
        object_list = self.get_context('object_list')
        self.assertEqual(len(object_list), 0)

    def test_un_grupo(self):
        grupo = factories.GrupoFactory()
        response = self.get('grupos:list')
        self.assertContains(response, grupo.nombre)

    def test_paginar(self):
        """Prueba que la vista no devuelva mas de 20 grupos por vez."""
        POR_PAGINA = 20
        factories.GrupoFactory.create_batch(POR_PAGINA + 1)
        self.get('grupos:list')
        object_list = self.get_context('object_list')
        self.assertEqual(len(object_list), POR_PAGINA)

    def test_orden(self):
        """Prueba que los resultados sean en orden alfabetico."""
        tres = factories.GrupoFactory(nombre='Grupo Nº3')
        uno = factories.GrupoFactory(nombre='Grupo Nº1')
        dos = factories.GrupoFactory(nombre='Grupo Nº2')
        expected = [uno, dos, tres]
        self.get('grupos:list')
        object_list = list(self.get_context('object_list'))
        self.assertEqual(object_list, expected)


class GrupoDetailViewTests(TestCase):
    def test_todos_atributos(self):
        """Verifica que se muestren todos los atributos del grupo."""
        # atributos a excluir de la vista de detalle
        EXCLUDE = ('id', 'slug', 'creado', 'actualizado')
        grupo = factories.GrupoFactory()
        response = self.get(grupo.get_absolute_url())
        for atributo, valor in vars(grupo).items():
            if not atributo.startswith('_') and atributo not in EXCLUDE:
                self.assertContains(response, valor, msg_prefix=atributo)

    def test_sin_secciones(self):
        grupo = factories.GrupoFactory()
        response = self.get(grupo.get_absolute_url())
        self.assertContains(
            response, 'No hay secciones. Por favor cree una nueva sección'
        )

    def test_una_seccion(self):
        grupo = factories.GrupoFactory()
        seccion = factories.SeccionFactory(grupo=grupo)
        response = self.get(grupo.get_absolute_url())
        self.assertContains(response, seccion.nombre)

    def test_10_secciones(self):
        grupo = factories.GrupoFactory()
        factories.SeccionFactory.create_batch(10, grupo=grupo)
        response = self.get(grupo.get_absolute_url())
        secciones = self.get_context('table').data
        for seccion in secciones:
            self.assertContains(response, seccion.nombre)


class GrupoUpdateViewTests(TestCase):
    def test_get_object(self):
        grupo = factories.GrupoFactory()
        self.get('grupos:update', slug=grupo.slug)
        self.assertContext('object', grupo)

    def test_editar(self):
        grupo = factories.GrupoFactory()
        data = {
            'nombre': 'Baden Powell',
        }
        self.post('grupos:update', slug=grupo.slug, data=data)
        self.response_302()
        grupo.refresh_from_db()
        self.assertEqual(grupo.nombre, 'Baden Powell')

    def test_falla_validacion(self):
        grupo = factories.GrupoFactory()
        self.post('grupos:update', slug=grupo.slug)
        self.response_200()


class GrupoCreateViewTests(TestCase):
    def test_get(self):
        self.get('grupos:create')
        self.response_200()

    def test_crear(self):
        grupo = factories.GrupoFactory.build()
        data = {
            k: v for k, v in vars(grupo).items() if not k.startswith('_')
        }
        self.post('grupos:create', data=data)
        self.assertEqual(models
                         .Grupo
                         .objects
                         .filter(nombre=grupo.nombre)
                         .count(), 1)


class SeccionDetailViewTests(TestCase):
    def test_todos_atributos(self):
        """Verifica que se muestren todos los atributos de la seccion."""
        # atributos a excluir de la vista de detalle
        EXCLUDE = ('id', 'slug', 'grupo_id')
        seccion = factories.SeccionFactory()
        response = self.get(seccion.get_absolute_url())
        for atributo, valor in vars(seccion).items():
            if not atributo.startswith('_') and atributo not in EXCLUDE:
                self.assertContains(response, valor, msg_prefix=atributo)


class SeccionCreateViewTests(TestCase):
    def test_get(self):
        grupo = factories.GrupoFactory()
        self.get('grupos:secciones:create', grupo_slug=grupo.slug)
        self.response_200()

    def test_crear(self):
        grupo = factories.GrupoFactory()
        data = {
            'rama': constants.SCOUT,
            'unidad': constants.MIXTA,
            'nombre': 'Baden Powell',
            'lema': 'Un lema agradable',
        }
        self.post('grupos:secciones:create', data=data, grupo_slug=grupo.slug)
        self.assertEqual(grupo.secciones.filter(**data).count(), 1)

    def test_grupo_en_contexto(self):
        grupo = factories.GrupoFactory()
        self.get('grupos:secciones:create', grupo_slug=grupo.slug)
        self.assertContext('grupo', grupo)


class SeccionUpdateViewTests(TestCase):
    def test_get_object(self):
        seccion = factories.SeccionFactory()
        self.get('grupos:secciones:update',
                 grupo_slug=seccion.grupo.slug,
                 slug=seccion.slug)
        self.assertContext('object', seccion)

    def test_editar(self):
        seccion = factories.SeccionFactory()
        grupo = seccion.grupo
        data = {
            'rama': constants.SCOUT,
            'unidad': constants.MIXTA,
            'nombre': 'Baden Powell',
            'lema': 'Un lema agradable',
        }
        self.post('grupos:secciones:update',
                  grupo_slug=seccion.grupo.slug,
                  slug=seccion.slug,
                  data=data)
        self.response_302()
        seccion.refresh_from_db()
        self.assertEqual(seccion.nombre, 'Baden Powell')
        self.assertEqual(seccion.lema, 'Un lema agradable')
        self.assertEqual(seccion.rama, constants.SCOUT)
        self.assertEqual(seccion.unidad, constants.MIXTA)
        self.assertEqual(seccion.grupo, grupo)

    def test_falla_validacion(self):
        seccion = factories.SeccionFactory()
        self.post('grupos:secciones:update',
                  grupo_slug=seccion.grupo.slug,
                  slug=seccion.slug)
        self.response_200()
