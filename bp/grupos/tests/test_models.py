from test_plus.test import TestCase

from .. import constants
from . import factories


class GrupoTests(TestCase):
    def test_get_absolute_url(self):
        grupo = factories.GrupoFactory()
        expected = f'/grupos/{grupo.slug}/'
        self.assertEqual(grupo.get_absolute_url(), expected)

    def test_str__con_numero(self):
        grupo = factories.GrupoFactory.build(nombre='Baden Powell', numero=42)
        self.assertEqual(str(grupo), 'Grupo Scout Nº42 - Baden Powell')

    def test_str__sin_numero(self):
        grupo = factories.GrupoFactory.build(nombre='San Martin', numero=None)
        self.assertEqual(str(grupo), 'Grupo Scout S/N - San Martin')

    def test_slug(self):
        grupo = factories.GrupoFactory(nombre='Grupo numero 1')
        self.assertEqual(grupo.slug, 'grupo-numero-1')


class SeccionTests(TestCase):
    def test_get_absolute_url(self):
        seccion = factories.SeccionFactory()
        expected = f'/grupos/{seccion.grupo.slug}/secciones/{seccion.slug}/'
        self.assertEqual(seccion.get_absolute_url(), expected)

    def test_str(self):
        seccion = factories.SeccionFactory.build(
            nombre='Baden Powell',
            rama=constants.ROVER,
            unidad=constants.MIXTA,
        )
        self.assertEqual(str(seccion), 'Baden Powell - Rover Unidad Mixta')

    def test_slug(self):
        seccion = factories.SeccionFactory(
            nombre='Baden Powell',
            rama=constants.SCOUT,
        )
        self.assertEqual(seccion.slug, 'scout-baden-powell')


    def test_get_update_url(self):
        seccion = factories.SeccionFactory()
        expected = (f'/grupos/{seccion.grupo.slug}/secciones/{seccion.slug}/'
                    '~editar/')
        self.assertEqual(seccion.get_update_url(), expected)
