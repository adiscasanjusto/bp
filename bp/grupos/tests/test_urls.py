from django.urls import reverse, resolve

from test_plus.test import TestCase



class TestGruposURLs(TestCase):
    """Test URL patterns for grupos app."""

    def test_detail_reverse(self):
        """grupos:detail should reverse to /grupos/<slug>/."""
        self.assertEqual(
            reverse('grupos:detail', kwargs={'slug': 'grupo-1'}),
            '/grupos/grupo-1/'
        )

    def test_detail_resolve(self):
        """/grupos/<slug>/ should resolve to grupos:detail."""
        self.assertEqual(resolve('/grupos/grupo-1/').view_name,
                         'grupos:detail')

    def test_list_reverse(self):
        """grupos:list should reverse to /grupos/."""
        self.assertEqual(reverse('grupos:list'), '/grupos/')

    def test_list_resolve(self):
        """/grupos/ should resolve to grupos:list."""
        self.assertEqual(resolve('/grupos/').view_name, 'grupos:list')

    def test_update_reverse(self):
        """grupos:update should reverse to /grupos/<slug>/~editar/."""
        self.assertEqual(
            reverse('grupos:update', kwargs={'slug': 'grupo-1'}),
            '/grupos/grupo-1/~editar/'
        )

    def test_update_resolve(self):
        """/grupos/<slug>/~editar/ should resolve to grupos:update."""
        self.assertEqual(resolve('/grupos/grupo-1/~editar/').view_name,
                         'grupos:update')

    def test_create_reverse(self):
        """grupos:create should reverse to /grupos/~nuevo/."""
        self.assertEqual(reverse('grupos:create'), '/grupos/~nuevo/')

    def test_create_resolve(self):
        """/grupos/~nuevo/ should resolve to grupos:create."""
        self.assertEqual(resolve('/grupos/~nuevo/').view_name,
                         'grupos:create')


class TestSeccionURLs(TestCase):
    """Test URL patterns for secciones."""

    def test_detail_reverse(self):
        """
        grupos:secciones:detail should reverse to
        /grupos/<grupo_slug>/secciones/<slug>/.
        """
        self.assertEqual(
            reverse('grupos:secciones:detail',
                    kwargs={'grupo_slug': 'grupo-1', 'slug': 'baden-powell'}),
            '/grupos/grupo-1/secciones/baden-powell/'
        )

    def test_detail_resolve(self):
        """/grupos/<grupo_slug>/secciones/<slug>/ should resolve to
        grupos:secciones:detail."""
        self.assertEqual(
            resolve('/grupos/grupo-1/secciones/baden-powell/').view_name,
            'grupos:secciones:detail'
        )

    def test_create_reverse(self):
        """
        grupos:secciones:create should reverse to
        /grupos/<grupo_slug>/secciones/~nuevo/.
        """
        self.assertEqual(
            reverse('grupos:secciones:create',
                    kwargs={'grupo_slug': 'grupo-1'}),
            '/grupos/grupo-1/secciones/~nuevo/'
        )

    def test_create_resolve(self):
        """/grupos/<grupo_slug>/secciones/~nuevo/ should resolve to
        grupos:secciones:create."""
        self.assertEqual(
            resolve('/grupos/grupo-1/secciones/~nuevo/').view_name,
            'grupos:secciones:create'
        )

    def test_update_reverse(self):
        """
        grupos:secciones:update should reverse to
        /grupos/<grupo_slug>/secciones/<slug>/~editar/.
        """
        self.assertEqual(
            reverse('grupos:secciones:update',
                    kwargs={'grupo_slug': 'grupo-1',
                            'slug': 'scout-baden-powell'}),
            '/grupos/grupo-1/secciones/scout-baden-powell/~editar/'
        )

    def test_update_resolve(self):
        """/grupos/<grupo_slug>/secciones/~nuevo/ should resolve to
        grupos:secciones:create."""
        url = '/grupos/grupo-1/secciones/scout-baden-powell/~editar/'
        self.assertEqual(resolve(url).view_name, 'grupos:secciones:update')
