# RAMAS
# ***************************************************************************
CASTOR = 1
ARCOIRIS = 2
LOBATO = 3
SCOUT = 4
RAIDER = 5
ROVER = 6

RAMAS = (
    (CASTOR, 'Castor'),
    (ARCOIRIS, 'Arcoiris'),
    (LOBATO, 'Lobato'),
    (SCOUT, 'Scout'),
    (RAIDER, 'Raider'),
    (ROVER, 'Rover'),
)

# UNIDADES
# ***************************************************************************
MIXTA = 0
FEMENINA = 1
MASCULINA = 2

UNIDADES = (
    (MIXTA, 'Mixta'),
    (FEMENINA, 'Femenina'),
    (MASCULINA, 'Masculina'),
)
