from django.urls import path

from . import views

app_name = "personas"
urlpatterns = [
    path('', views.PersonaListView.as_view(), name='list'),
    path('<slug:grupo_slug>/~nueva/', views.PersonaCreateView.as_view(),
         name='create'),
    path('<slug:slug>/', views.PersonaDetailView.as_view(), name='detail'),
    path('<slug:slug>/~editar/', views.PersonaUpdateView.as_view(),
         name='update'),
]
