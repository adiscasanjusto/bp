from django.urls import reverse, resolve

from test_plus.test import TestCase


class TestPersonaURLs(TestCase):
    """Test URL patterns for personas app."""

    def test_detail_reverse(self):
        """personas:detail should reverse to /personas/<slug>/."""
        self.assertEqual(
            reverse("personas:detail", kwargs={"slug": "perez-juan-1"}),
            "/personas/perez-juan-1/",
        )

    def test_detail_resolve(self):
        """/personas/<slug>/ should resolve to personas:detail."""
        self.assertEqual(
            resolve("/personas/testuser/").view_name, "personas:detail"
        )

    def test_list_reverse(self):
        """personas:list should reverse to /personas/."""
        self.assertEqual(reverse("personas:list"), "/personas/")

    def test_list_resolve(self):
        """/personas/ should resolve to personas:list."""
        self.assertEqual(resolve("/personas/").view_name, "personas:list")

    def test_update_reverse(self):
        """personas:update should reverse to /personas/<slug>/~editar/."""
        self.assertEqual(
            reverse("personas:update", kwargs={"slug": "perez-juan-1"}),
            "/personas/perez-juan-1/~editar/",
        )

    def test_update_resolve(self):
        """/personas/<slug>/~editar/ should resolve to personas:update."""
        self.assertEqual(
            resolve("/personas/perez-juan-1/~editar/").view_name,
            "personas:update",
        )

    def test_create_reverse(self):
        """personas:create should reverse to /personas/<grupo_slug>/~nueva/."""
        self.assertEqual(
            reverse("personas:create", kwargs={"grupo_slug": "grupo-1"}),
            "/personas/grupo-1/~nueva/",
        )

    def test_create_resolve(self):
        """/personas/~create/ should resolve to personas:create."""
        self.assertEqual(
            resolve("/personas/grupo-1/~nueva/").view_name,
            "personas:create",
        )
