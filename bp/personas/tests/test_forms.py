from test_plus.test import TestCase

from bp.grupos.tests import factories

from .. import forms


class PersonaFormTests(TestCase):
    """
    Pruebas del formulario de creacion/edicion de personas."""
    def test_solo_mostrar_secciones_del_grupo(self):
        """Solo debe mostrar las secciones del grupo pasado por parametro."""
        seccion1 = factories.SeccionFactory()
        grupo = seccion1.grupo
        # seccion2 no se debe mostrar porque pertenece a otro grupo
        seccion2 = factories.SeccionFactory()
        form = forms.PersonaForm(grupo=grupo)
        self.assertIn(seccion1, form['seccion'].field.queryset)
        self.assertNotIn(seccion2, form['seccion'].field.queryset)

    def test_solo_guardar_seccion_del_grupo(self):
        """Solo debe permitir guardar una persona en la seccion del grupo
        pasado por parametro.
        """
        seccion1 = factories.SeccionFactory()
        grupo = seccion1.grupo
        # seccion2 no se debe poder guardar porque pertenece a otro grupo
        seccion2 = factories.SeccionFactory()
        data = {
            'apellido': 'bla',
            'nombre': 'bla',
            'seccion': seccion2.id,
        }
        form = forms.PersonaForm(grupo=grupo, data=data)
        self.assertIn('seccion', form.errors)
