from test_plus.test import TestCase

from . import factories


class PersonaTests(TestCase):
    def test_get_absolute_url(self):
        persona = factories.PersonaFactory()
        expected = f'/personas/{persona.slug}/'
        self.assertEqual(persona.get_absolute_url(), expected)

    def test_nombre_completo(self):
        persona = factories.PersonaFactory.build(nombre='Juan',
                                                 apellido='Perez')
        self.assertEqual(persona.nombre_completo, 'PEREZ, Juan')

    def test_str(self):
        persona = factories.PersonaFactory.build(nombre='Juan',
                                                 apellido='Perez')
        self.assertEqual(str(persona), 'PEREZ, Juan')
