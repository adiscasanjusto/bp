from test_plus.test import TestCase

from bp.grupos.tests import factories as grupos_factories

from .. import models
from . import factories


class PersonaListViewTests(TestCase):
    def test_lista_vacia(self):
        self.get("personas:list")
        object_list = self.get_context("object_list")
        self.assertEqual(len(object_list), 0)

    def test_una_persona(self):
        persona = factories.PersonaFactory()
        response = self.get("personas:list")
        self.assertContains(response, persona.nombre_completo)

    def test_paginar(self):
        """Prueba que la vista no devuelva mas de 20 personas por vez."""
        POR_PAGINA = 20
        factories.PersonaFactory.create_batch(POR_PAGINA + 1)
        self.get("personas:list")
        object_list = self.get_context("object_list")
        self.assertEqual(len(object_list), POR_PAGINA)

    def test_orden(self):
        """Prueba que los resultados sean en orden alfabetico."""
        uno = factories.PersonaFactory(apellido="Avila", nombre="Lucrecia")
        tres = factories.PersonaFactory(apellido="Fernandez")
        dos = factories.PersonaFactory(apellido="Avila", nombre="Venicio")
        expected = [uno, dos, tres]
        self.get("personas:list")
        object_list = list(self.get_context("object_list"))
        self.assertEqual(object_list, expected)


class PersonaDetailViewTests(TestCase):
    def test_muestra_nombre_completo(self):
        persona = factories.PersonaFactory()
        response = self.get(persona.get_absolute_url())
        self.assertContains(response, persona.nombre_completo)


class PersonaUpdateViewTests(TestCase):
    def test_get_object(self):
        persona = factories.PersonaFactory()
        self.get("personas:update", slug=persona.slug)
        self.assertContext("object", persona)

    def test_editar(self):
        persona = factories.PersonaFactory()
        data = {"nombre": "juan", "apellido": "perez", "celular": "1122334455"}
        self.post("personas:update", slug=persona.slug, data=data)
        self.response_302()
        persona.refresh_from_db()
        self.assertEqual(persona.nombre, "juan")
        self.assertEqual(persona.apellido, "perez")
        self.assertEqual(persona.celular, "1122334455")

    def test_falla_validacion(self):
        persona = factories.PersonaFactory()
        data = {}
        self.post("personas:update", slug=persona.slug, data=data)
        self.response_200()


class PersonaCreateViewTests(TestCase):
    def test_get(self):
        grupo = grupos_factories.GrupoFactory()
        self.get("personas:create", grupo_slug=grupo.slug)
        self.response_200()

    def test_crear(self):
        """Crear persona para un determinado grupo."""
        grupo = grupos_factories.GrupoFactory()
        data = {"apellido": "Perez", "nombre": "Juan", "dni": "12345678"}
        self.post("personas:create", data=data, grupo_slug=grupo.slug)
        self.response_302()
        self.assertTrue(models.Persona.objects.filter(**data).exists())
