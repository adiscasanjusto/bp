import factory

from bp.grupos.tests import factories as grupos_factories

from .. import models


class PersonaFactory(factory.django.DjangoModelFactory):
    """ Crea instancias de personas."""
    nombre = factory.Faker('first_name')
    apellido = factory.Faker('last_name')
    dni = factory.Faker('pyint')
    fecha_nacimiento = factory.Faker('past_date')
    seccion = factory.SubFactory(grupos_factories.SeccionFactory)
    grupo = factory.SubFactory(grupos_factories.GrupoFactory)
    tipo_miembro = factory.Iterator([x[0] for x in models.TIPO_MIEMBROS])
    email = factory.Faker('email')
    telefono = factory.Faker('phone_number')
    celular = factory.Faker('phone_number')
    direccion = factory.Faker('address')

    class Meta:
        model = 'personas.Persona'
