from django import shortcuts
from django.views import generic

import django_tables2

from bp.grupos.models import Grupo

from . import forms, models, tables


class PersonaListView(django_tables2.SingleTableView):
    """Muestra lista de personas."""

    model = models.Persona
    table_class = tables.PersonaTable
    paginate_by = 20
    ordering = ("apellido", "nombre")


class PersonaDetailView(generic.DetailView):
    """Muestra detalles de una persona"""

    model = models.Persona


class PersonaUpdateView(generic.UpdateView):
    """Edita datos de una persona."""

    model = models.Persona
    form_class = forms.PersonaForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["grupo"] = self.object.grupo
        return kwargs


class PersonaCreateView(generic.CreateView):
    """Crea una persona nueva."""

    model = models.Persona
    form_class = forms.PersonaForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["grupo"] = self.get_grupo()
        return kwargs

    def get_grupo(self):
        return shortcuts.get_object_or_404(
            Grupo, slug=self.kwargs["grupo_slug"]
        )
