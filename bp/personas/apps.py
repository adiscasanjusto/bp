from django.apps import AppConfig


class PersonasConfig(AppConfig):
    name = "bp.personas"
    verbose_name = "Personas"

    def ready(self):
        """Override this to put in:
            Personas system checks
            Personas signal registration
        """
        try:
            import personas.signals  # noqa F401
        except ImportError:
            pass
