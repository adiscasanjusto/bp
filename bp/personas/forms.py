import datetime

from django import forms

from . import models


year = datetime.date.today().year


class PersonaForm(forms.ModelForm):
    class Meta:
        model = models.Persona
        fields = (
            "apellido",
            "nombre",
            "dni",
            "fecha_nacimiento",
            "seccion",
            "tipo_miembro",
            "email",
            "telefono",
            "celular",
            "direccion",
        )
        widgets = {
            "fecha_nacimiento": forms.SelectDateWidget(
                years=range(year - 4, year - 80, -1)
            )
        }

    def __init__(self, grupo, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.grupo = grupo
        self.fields["seccion"].queryset = self.grupo.secciones.all()
