import django_tables2 as tables

from . import models


class PersonaTable(tables.Table):
    nombre_completo = tables.LinkColumn(order_by=('apellido', 'nombre'))

    class Meta:
        model = models.Persona
        fields = ('nombre_completo', 'dni', 'fecha_nacimiento', 'email')
