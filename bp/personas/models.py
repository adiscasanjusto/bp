from django.db import models
from django.urls import reverse

import autoslug


BENEFICIARIO = 'BENEFICIARIO'
ACTIVO = 'ACTIVO'
ADHERENTE = 'ADHERENTE'

TIPO_MIEMBROS = (
    (BENEFICIARIO, 'BENEFICIARIO/A'),
    (ACTIVO, 'ACTIVO/A'),
    (ADHERENTE, 'ADHERENTE'),
)


class Persona(models.Model):
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    slug = autoslug.AutoSlugField(populate_from='nombre_completo')
    dni = models.CharField(blank=True, max_length=255)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    seccion = models.ForeignKey('grupos.Seccion', related_name='personas',
                                null=True, blank=True,
                                on_delete=models.SET_NULL)
    grupo = models.ForeignKey('grupos.Grupo', related_name='personas',
                              null=True, blank=True,
                              on_delete=models.SET_NULL)
    # TODO add cargo (app asociaciones) many to many
    tipo_miembro = models.CharField(choices=TIPO_MIEMBROS, max_length=12,
                                    blank=True)
    email = models.EmailField(blank=True)
    telefono = models.CharField(max_length=32, blank=True)
    celular = models.CharField(max_length=32, blank=True)
    direccion = models.CharField(max_length=255, blank=True)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre_completo

    @property
    def nombre_completo(self):
        return f'{self.apellido.upper()}, {self.nombre}'

    def get_absolute_url(self):
        return reverse('personas:detail', kwargs={'slug': self.slug})
