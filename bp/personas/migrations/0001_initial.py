# Generated by Django 2.0.4 on 2018-04-22 19:36

import autoslug.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=255, verbose_name='Nombre')),
                ('apellido', models.CharField(blank=True, max_length=255, verbose_name='Apellido')),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='nombre_completo')),
            ],
        ),
    ]
