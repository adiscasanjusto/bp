#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace


celery -A bp.taskapp worker -l INFO
