#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A bp.taskapp worker -l INFO
